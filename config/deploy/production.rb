server '157.245.12.116', user: 'deploy', roles: %w{web app db}

set :application, 'video_conferencing_rails'
set :repo_url, 'git@bitbucket.org:tallent16/videoconferencingrails.git' # Edit this to match your repository
set :branch, :master
set :deploy_to, '/home/deploy/apps/video_conferencing_rails'
set :pty, true
set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets public/packs .bundle node_modules vendor/bundle public/system public/uploads}
set :keep_releases, 5
set :rvm_type, :user
set :rvm_ruby_version, '2.6.0' # Edit this if you are using MRI Ruby

set :puma_rackup, -> { File.join(current_path, 'config.ru') }
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_bind, "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"    #accept array for multi-bind
set :puma_conf, "#{shared_path}/puma.rb"
set :puma_access_log, "#{shared_path}/log/puma_error.log"
set :puma_error_log, "#{shared_path}/log/puma_access.log"
set :puma_role, :app
set :puma_env, fetch(:rack_env, fetch(:rails_env, 'production'))
set :puma_threads, [0, 8]
set :puma_workers, 0
set :puma_worker_timeout, nil
set :puma_init_active_record, true
set :puma_preload_app, false


set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_bind, "unix://#{shared_path}/tmp/sockets/puma.sock"


before "deploy:assets:precompile", "deploy:yarn_install"
namespace :deploy do
  desc "Run rake yarn install"
  task :yarn_install do
    on roles(:web) do
      within release_path do
        execute("cd #{release_path} && yarn install --silent --no-progress --no-audit --no-optional")
      end
    end
  end
end


 # set :ssh_options, {
 #   # user: "ubuntu",
 #   keys: %w(/home/netset/vasu/lone_worker_new_by_vasu/lone_worker.pem),
 #   forward_agent: true,
 #   auth_methods: %w(publickey)
 #   # password: "please use keys"
 # }

# after "deploy", "swagger:docs"

# namespace :deploy do

#   after :updated, :notify do
#     puts "ffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
#      exec "rake swagger:docs"
#   end
# end
