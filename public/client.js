// getting dom elements
var inputRoom = document.getElementById('room');
var btnEnter = document.getElementById('enter');
var divSelectRoom = document.getElementById('selectRoom');
var divVideos = document.getElementById('videos');

// variables and constants
var apiKey;
var sessionId;
var token;
var roomName;
var SERVER_BASE_URL = 'http://localhost:3000';

// Let's do this
btnEnter.onclick = function () {
    if (inputRoom.value === '') {
        alert('Please type a room name');
    } else {

            apiKey = "46717132";
            sessionId = "1_MX40NjcxNzEzMn5-MTU4ODU5MTQ1MTg5Nn5LdTIwUXMxUGl5WHMvdjE3V1BiNWJSYWV-UH4";
            token = "T1==cGFydG5lcl9pZD00NjcxNzEzMiZzaWc9ZjgzZWY1MGFiNjgzYTM3OTk0YWYwMGUzODkwMTg3ZDljNjkwOWFkMTpyb2xlPXB1Ymxpc2hlciZzZXNzaW9uX2lkPTFfTVg0ME5qY3hOekV6TW41LU1UVTRPRFU1TVRRMU1UZzVObjVMZFRJd1VYTXhVR2w1V0hNdmRqRTNWMUJpTldKU1lXVi1VSDQmY3JlYXRlX3RpbWU9MTU4ODYwMTIxOSZub25jZT0wLjM0NzE3Mzc2MjQ0NzM3MTY0";
            initializeSession();
     }
}

// tokbox code
function initializeSession() {

    divSelectRoom.style = "display: none";
    divVideos.style = "display: block";

    var session = OT.initSession(apiKey, sessionId);

    // Subscribe to a newly created stream
    session.on('streamCreated', function (event) {
        session.subscribe(event.stream, 'subscribers', {
            insertMode: 'append',
            width: '360px',
            height: '240px'
        }, handleError);
    });
    // Create a publisher
    var publisher = OT.initPublisher('publisher', {
        insertMode: 'append',
        width: '360px',
        height: '240px'
    }, handleError);

    // Connect to the session
    session.connect(token, function (error) {
        // If the connection is successful, publish to the session
        if (error) {
            handleError(error);
        } else {
            session.publish(publisher, handleError);
        }
    });
}

// Handling all of our errors here by alerting them
function handleError(error) {
    if (error) {
        alert(error.message);
    }
}